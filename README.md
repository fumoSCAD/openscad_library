# OpenSCAD_Library

**Work in progress, frequent changes possibe**

During my work with OpenSCAD, I found I use several operators and modules frequently. Since these operators and modules are not natively available, I built a library with those frequently required operators and components, especially for building function models.

[TOC]

## Operators
### circleof
**Syntax:** circleof(r, center, num, startAngle, deltaAngle)

**Function:** Arranges a set of objects on a circle or an arc of radius r in the xy-plane. 

|**Parameter**|**Description**|
|----|----|
| r | radius of the circle (default 10) |
| center | coordinates of the circle's center (default [0,0,0]) |	
| num	| number of objects to arrange (default 6) |
| startAngle | starting angle (default 0) |
| deltaAngle | angle between arranged objects (default zero, leading to equally angular spaced objects) |


**Examples:**

Circle of small cylinders:  
circleof(r=20) cylinder(d=5, h=10);

![Circle of 6 cylinders](./images/baConEl_circleOf_default.png)

Square arrangement (e.g. for fixing stepper motors)
circleof(r=40/sqrt(2), num=4, startAngle=45) cylinder(d=5, h=10);

![](./images/baConEl_circleOf_square.png)

### rowof
**Syntax:** rowof(num, distance, start, dir)

**Function:**  Arranges a set of object in a row.

|**Parameter**|**Description**|
|----|----|
| num	| number of objects to arrange (default 6) |
| distance | distance between the objects (default 10) |
| start | start position (default [0,0,0]) |
| dir | vector specifying the direction of the row (default [1,0,0], meaning x-axis) |

**Example:**
Row of cylinders  
rowof(num=3, distance=30, dir=[1,1,0]) cylinder(d=4,h=16, center=true);

![](./images/baConEl_rowof.png)

### rotateAboutPoint
**Syntax:** rotateAboutPoint(rotAngle, rotAxisVector, rotCenterPoint)

**Function:**  Rotates an object by *rotAngle* around an arbitrary axis specified by the axis' direction vector *rotAxisVector* and the center point of the rotation given in *rotCenterPoint*.

**Example:**

rotateAboutPoint(rotAngle=-32.5, rotAxisVector=[0,1,0], rotCenterPoint=[10,0,30]) cube([20,30,40]);

![](./images/baConEl_arbitraryRotation.png)

## Components
### luProfile
Generates the most common profiles in construction.

**Syntax:** luProfile (hLeft, width, hRight, length, thick)

|**Parameter**|**Description**|
|----|----|
|hLeft| height of left part of the profile (may be 0 for L-shaped profile, default 20) |
|width| (outer) width of the profile (default 30) |
|hRight| height of right part of the profile (may be 0 for L-shaped profile, default 20)|
|length| profile length (default 40) |
|thick|profile thickness (default 5)|

**Examples:**

Asymmetric U-Profile:  
luProfile (hLeft=40, width=30, hRight=20, length=40, thick=4)

![](./images/baConEl_luProfile.png)

L-Profile:  
luProfile (hLeft=0, width=40, hRight=20, length=40, thick=3);

![](./images/baConEl_luProfile_0_40_20_th3.png)

### boxProfile
Also known as rectangular tube.

**Syntax:** boxProfile(width, height, length, thick)

|**Parameter**|**Description**|
|----|----|
|width| (outer) width of the profile (default 40) |
|height| height of the profile (default 20)|
|length| profile length (default 50) |
|thick|profile thickness (default 5)|

**Example:**

boxProfile(width=50, height=40, length=50, thick=4);

![](./images/baConEl_boxProfile.png)


### xProfile
A rarely used, more rugged profile.

**Syntax:** boxProfile(width, height, length, thick)

|**Parameter**|**Description**|
|----|----|
|width| (outer) width of the profile (default 30) |
|height| height of the profile (default 30)|
|length| profile length (default 50) |
|thick|profile thickness (default 4)|

**Example:**

xProfile(width=30, height=30, length=50, thick=4);

![](./images/baConEl_xProfile.png)

### wedge
Useful for enforcement of edges or just to give objects a smooth shape.

**Examples:**

Triangular wedge:  
wedge(width=10, height=10, length=50, type=0);

![Triangular wedge](./images/baConEl_wedge_type0.png)

Circular wedge:  
wedge(width=10, height=10, length=50, type=1);

![](./images/baConEl_wedge_type1.png)

### tube
Often needed, self-explaining

**Syntax:** tube(di, do, length,  wall)

You must either specify inner and outer diameter or one of the diameters together with the wall thickness

|**Parameter**|**Description**|
|----|----|
|di| inner diameter |
|do| outer diameter|
|length| tube length (default 10) |
|wall| tube wall thickness |

**Examples:**

tube(di=6.4, do=10, length=1); // a washer

![](./images/baConEl_tube_washer.png)

tube(do=20, length=25, wall=3); // tube specified by outer diameter and wall thickness.

![](./images/baConEl_tube.png)

### donut

**Syntax:** donut(di, do, ri=0, ro=0)
You may either specify the inner/outer diameters or radii.

**Examples:**

donut(di=20, do=30);

![donut](./images/baConEl_donut.png)

scale([1,1,4]) donut(di=40, do=50);

![donut](./images/baConEl_donut_scaled.png)


### recTube
Alias for [boxProfile](###boxProfile).

### Hole
The hole-module is basically a cylinder, but can be oversized a little to compensate for too narrow holes caused by the 3d-pronting process and/or to allow a snug fit of any cylindrical element in the hole.

**Syntax:** hole(d=4, h=10, holeCorrect=0.4)

|**Parameter**|**Description**|
|----|----|
|d| nominal hole diameter |
|h| hole depth|
|holeCorrect| diameter oversize (default 0.4) |

